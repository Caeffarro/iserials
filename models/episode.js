var mongoose = require('mongoose');
var Schema = mongoose.Schema;

/* The user schema attributes */
var EpisodeSchema = new Schema({
   serial: String,
   season: String,
   episod: String,
   url: String,
   img: String,
   seasons: String,
   descr: String,
   title: String
});

module.exports = mongoose.model('Episode', EpisodeSchema);