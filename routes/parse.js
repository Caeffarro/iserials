var router = require('express').Router();
var request = require('request');
var cheerio = require('cheerio');
var utf8 = require('utf8');

var Episode = require('../models/episode');

function getNumberOfPages(a_url, callback) {
	request(a_url, function (error, response, body) {
		if (error) {
	        console.log('Не удалось получить страницу из за следующей ошибки: ' + error);
	        return;
	    }
	    var items= [];
	    var a_pages = 0;
	    var $ = cheerio.load(body),
		a_pages = $('.serial-page-nav a:nth-last-child(2)').text();
		var a_seasons = $('.serial-page-nav ul li:last-child span').text();
		var a_descr = $('.serial-page-text[itemprop="description"] p:first-child').text();
		var a_title = $('h1[itemprop="name"]').text();
		items.push(a_pages);
		items.push(a_seasons);
		items.push(utf8.encode(a_descr));
		items.push(utf8.encode(a_title));
	    callback(items);
	});
}
router.get('/parse', function (req, res, next) {
	var serial = req.query.serial

	Episode.find({serial: serial}).remove().exec();

	var url = 'http://fanserials.tv/'+serial+'/';
	var minus = serial.split('-').length;
	var pages = 1;
	console.log('URL: '+url);
	getNumberOfPages(url, function(p_items){
  		pages = p_items[0];
  		var seasons = p_items[1];
  		var descr = p_items[2]; 
  		var title = p_items[3];
  		for (var index = 1; index <= pages; index++) {
		var _url = url + 'page/' + index + '/';
		request(_url, function (error, response, body) {
		    if (error) {
		        console.log('Не удалось получить страницу из за следующей ошибки: ' + error);
		        return;
		    }
		    var $ = cheerio.load(body),
		        links = $('.episode-item a[itemprop="url"]');
		    links.each(function (i, link) {
		    // получаем атрибуты href для каждой ссылки
		        var url1 = $(link).attr("href");
		        var img = $('.episode-item-body', $(this)).attr("style");
		        img = 'background: url("' + img.slice(img.indexOf('=')+1, img.indexOf('&'));
		        img += '"); height: 100%;';
		    // обрезаем ненужный мусор
		        url1 = url1.replace("/url?q=", "").split("&")[0];
		        if (url1.charAt(0) === "/") {
		            return;
		        }
		        var episode = new Episode();
	            episode.url = url1;
	            var arr = url1.split('-');
	            episode.serial = serial;
	            episode.season = arr[1+minus];
	            episode.episod = arr[3+minus];
	            episode.img = img;
	            episode.seasons = seasons;
	            episode.descr = descr;
	            episode.title = title;
	            console.log('episode: ' + episode);
	            episode.save(function (err, episode) {
	                if (err) return 
	                	next(err);
	            });
			});
		});
	}
	});

	res.redirect('/');
});

module.exports = router;