var router = require('express').Router();
var User = require('../models/user');
var passport = require('passport');
var passportConf = require('../config/passport');
var async = require('async');

router.get('/login', function (req, res, next) {
    if (req.user) 
    	return res.redirect('/logout');
    res.render('accounts/login', {message: req.flash('loginMessage')});
});


router.post('/login', passport.authenticate('local-login', {
    successRedirect: '/',
    failureRedirect: '/login',
    failureFlash: true
}));

router.get('/signup', function (req, res, next) {
    res.render('accounts/signup', {
        errors: req.flash('errors')
    });
});

router.post('/signup', function (req, res, next) {
    async.waterfall([
        function (callback) {
            var user = new User();
            user.profile.name = req.body.name;
            user.email = req.body.email;
            user.password = req.body.password;
            User.findOne({email: req.body.email}, function (err, existingUser) {
                if (err) return next(err);
                if (existingUser) {
                    req.flash('errors', 'Account with that email already exists');
                    return res.redirect('/signup');
                } else {
                    user.save(function (err, user) {
                        if (err) return next(err);
                        callback(null, user);
                    });
                }
            });
        },
        function (user) {
            req.logIn(user, function (err) {
                if (err) return next(err);
                res.redirect('/');
            });
        }
    ]);
});

router.get('/logout', function (req, res, next) {
    req.logout();
    res.render('accounts/signup', {
        errors: req.flash('errors')
    });
});

module.exports = router;