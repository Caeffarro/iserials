var router = require('express').Router();
var utf8 = require('utf8');

var Episode = require('../models/episode');

router.get('/serial/:name', function (req, res, next) {
	var name =  req.params.name;
	Episode.find({'serial': name}).exec(function (err, episodes) {
        if (err) return 
        	next(err);
        res.render('page/revewser', {
            data: episodes,
            serial: name,
            seasons: episodes[0].seasons,
            descr: utf8.decode(episodes[0].descr),
            title: utf8.decode(episodes[0].title)
		});
	});
});

module.exports = router;