var router = require('express').Router();

var User = require('../models/user');

router.get('/', function (req, res, next) {
    res.render('page/main');
});

router.get('/favourites', function (req, res, next) {
	if (req.user) {
        res.render('page/favourites', {
        	user: req.user
        });
    } else {
    	res.redirect('/login');
    }
});

router.get('/add/:name', function (req, res, next) {
	if (req.user) {
		var name =  req.params.name;
		if (name == 'flash') {
			User.updateOne(
				{email: req.user.email}, 
				{$set: {favourites :{flash: true}}},
				function (err, user) {
					if (err) 
						return next(err);
                });
		} else if (name == 'arrow') {
			User.updateOne(
				{email: req.user.email}, 
				{$set: {favourites :{arrow: true}}},
				function (err, user) {
					if (err) 
						return next(err);
                });
		} else if (name == 'doctor-who') {
			User.updateOne(
				{email: req.user.email}, 
				{$set: {favourites :{doctor_who: true}}},
				function (err, user) {
					if (err) 
						return next(err);
                });
		} else if (name == 'daredevil') {
			User.updateOne(
				{email: req.user.email}, 
				{$set: {favourites :{daredevil: true}}},
				function (err, user) {
					if (err) 
						return next(err);
                });
		} else if (name == 'agents-of-shield') {
			User.updateOne(
				{email: req.user.email}, 
				{$set: {favourites :{agents_of_shield: true}}},
				function (err, user) {
					if (err) 
						return next(err);
                });
		} else if (name == 'supernatural') {
			User.updateOne(
				{email: req.user.email}, 
				{$set: {favourites :{supernatural: true}}},
				function (err, user) {
					if (err) 
						return next(err);
                });
		} else {

		}
		res.redirect('/');
	} else {
		res.redirect('/login');
	}
});

module.exports = router;